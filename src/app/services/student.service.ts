import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Student } from '../models/Student';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StudentService {
  // what is the url in the BACKEND for our GetMapping?
  // http://localhost:8080/api/s1/students

  private getUrl: string = "http://localhost:8080/api/s1/students"

  // using dependency injection to bring in the httpClient
  constructor(private httpClient: HttpClient) { }

  // create a method that will make a request to the GetMapping method in our backend
  getStudents(): Observable<Student[]> {
    return this.httpClient.get<Student[]>(this.getUrl).pipe(
      map(result => result)
    )
  }

  saveStudent(newStudent: Student): Observable<Student> {
    return this.httpClient.post<Student>(this.getUrl, newStudent);
  }


  // method that will make a request to the GetMapping method
  // to view an individual post from our backend
  viewStudent(id: number): Observable<Student> {
    return this.httpClient.get<Student>(`${this.getUrl}/${id}`).pipe(
      map(result => result)
    )
  }

  // method that will make a request to the delete mapping method
  // to delete an individual student from our backend
  deleteStudent(id: number): Observable<any> {
    return this.httpClient.delete(`${this.getUrl}/${id}`, {responseType: 'text'})
  }

}
