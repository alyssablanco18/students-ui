import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Student } from 'src/app/models/Student';
import { StudentService } from 'src/app/services/student.service';

@Component({
  selector: 'app-add-student',
  templateUrl: './add-student.component.html',
  styleUrls: ['./add-student.component.css']
})
export class AddStudentComponent implements OnInit {

  // properties
  newStudent: Student = {
    id: 0,
    firstName:"",
    lastName:"",
    studentID: "",
    email:"",
    gradeLevel: 0,
    dateOfBirth: "",
    emergencyContactNumber:""
  }

  // dependency injection
  constructor(private studentSvc: StudentService,
              private router: Router,
              private activeRoute: ActivatedRoute) { }


  ngOnInit(): void {
    var isIdPresent = this.activeRoute.snapshot.paramMap.has("id");
    if (isIdPresent) {
      const id = this.activeRoute.snapshot.paramMap.get("id");
      this.studentSvc.viewStudent(Number(id)).subscribe(
        (data: Student) => this.newStudent = data
      )
    }
  }

  // method that will make a request to the saveStudent in our service
  // and will redirect the user to /students aka our home page
  savedStudent() {
    this.studentSvc.saveStudent(this.newStudent).subscribe(
      data => {
        this.router.navigateByUrl("/students");
      }
    )
  }

  // delete student method
  deletedStudent(id: number) {
    this.studentSvc.deleteStudent(id).subscribe(
      data => this.savedStudent()
    )
  }

}
