import { Component, OnInit } from '@angular/core';
import { Student } from 'src/app/models/Student';
import { StudentService } from 'src/app/services/student.service';

@Component({
  selector: 'app-list-students',
  templateUrl: './list-students.component.html',
  styleUrls: ['./list-students.component.css']
})
export class ListStudentsComponent implements OnInit {
  // property
  students: Student[] = [];

  // dependency injection, bring it to our service
  constructor(private studentSvc: StudentService) { }

  ngOnInit(): void {
    // call the listStudents()
    this.listStudents();
  }

  // method that will display the list of our students/data
  listStudents() {
    this.studentSvc.getStudents().subscribe(
      data => this.students = data
    )
  }

  // method that will delete the data using the deleteStudent method
  // from our service
  deletedStudent(id: number) {
    this.studentSvc.deleteStudent(id).subscribe(
      data => this.listStudents()
    )
  }

}
